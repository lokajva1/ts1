package shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class StandardItemTest {

    private StandardItem standardItem;

    @BeforeEach
    public void createStandardItem(){
        this.standardItem = new StandardItem(1, "Apple", 1, "Fruits", 666);
    }

    @Test
    public void StandardItem_constructorLoyaltyPoints_shouldPass() {
//        given
        int expectedLoyaltyPoints = 666;
//        when
//        then
        assertEquals(expectedLoyaltyPoints, standardItem.getLoyaltyPoints());
    }

    @Test
    public void copy_makingDeepCopyOfItem_shouldPass() {
//        given
//        when
        StandardItem copyOfStandardItem = standardItem.copy();
//        then
        assertEquals(standardItem.getLoyaltyPoints(), copyOfStandardItem.getLoyaltyPoints());
    }

    @Test
    public void copy_makingDeepCopyOfItem_shouldFail() {
//        given
        int newLoyaltyPoints = 69;
//        when
        StandardItem copyOfStandardItem = standardItem.copy();
        standardItem.setLoyaltyPoints(newLoyaltyPoints);
//        then
        assertNotEquals(standardItem.getLoyaltyPoints(), copyOfStandardItem.getLoyaltyPoints());
    }

    @ParameterizedTest
    @MethodSource("equalsGenerator")
    public void equals_compareTwoItems_shouldPass(StandardItem standardItem, boolean expected) {
//        given
        StandardItem equalStandardItem = new StandardItem(1, "Apple", 1, "Fruits", 666);
//        when
        boolean actual = standardItem.equals(equalStandardItem);
//        then
        assertEquals(expected, actual);
    }

    private static Stream<Arguments> equalsGenerator() {
        return Stream.of(
                Arguments.of(new StandardItem(1, "Apple", 1, "Fruits", 666), true),
                Arguments.of(new StandardItem(2, "Watermelon", 2, "Vegetable", 69), false));
    }
}
