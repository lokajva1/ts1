package shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {

    private static final String CUSTOMER_NAME = "Sherlock Holmes";
    private static final String CUSTOMER_ADDRESS = "10 Downing Street";
    private static final int STATE = 251;

    private ShoppingCart shoppingCart;

    @BeforeEach
    private void creteShoppingCart() {
        ArrayList<Item> items = new ArrayList<>(List.of(
                new StandardItem(1, "Apple", 1, "Fruits", 666),
                new StandardItem(2, "Watermelon", 2, "Vegetable", 69)
        ));
        shoppingCart = new ShoppingCart(items);
    }

    /*
    TODO jakoby ten konstruktor je napsany blbe takze jsem to upravil tak aby vyhazoval NullPointerException,
        ale v realu by asi bylo potreba zalozit bug aby se to vyresilo nejak elegantneji.
     */

    @Test
    public void Order_setItemsToNull_shouldThrowNullPointerException() {
//        given
        shoppingCart = null;
//        when
//        then
        assertThrows(NullPointerException.class, () -> new Order(shoppingCart, CUSTOMER_NAME, CUSTOMER_ADDRESS));
    }

    @Test
    public void Order_setItemsToNullWithState_shouldThrowNullPointerException() {
//        given
        shoppingCart = null;
//        when
//        then
        assertThrows(NullPointerException.class, () -> new Order(shoppingCart, CUSTOMER_NAME, CUSTOMER_ADDRESS, STATE));
    }

    @Test
    public void Order_setItemsToNotNull_shouldPass() {
//        given
//        when
        Order order = new Order(shoppingCart, CUSTOMER_NAME, CUSTOMER_ADDRESS);
//        then
        assertEquals(shoppingCart.items, order.getItems());
        assertEquals(CUSTOMER_NAME, order.getCustomerName());
        assertEquals(CUSTOMER_ADDRESS, order.getCustomerAddress());
    }

    @Test
    public void Order_setItemsToNotNullWithState_shouldPass() {
//        given
//        when
        Order order = new Order(shoppingCart, CUSTOMER_NAME, CUSTOMER_ADDRESS, STATE);
//        then
        assertEquals(shoppingCart.items, order.getItems());
        assertEquals(CUSTOMER_NAME, order.getCustomerName());
        assertEquals(CUSTOMER_ADDRESS, order.getCustomerAddress());
        assertEquals(STATE, order.getState());
    }
}
