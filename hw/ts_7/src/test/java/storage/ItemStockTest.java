package storage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import shop.Item;
import shop.StandardItem;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemStockTest {

    private ItemStock itemStock;

    @BeforeEach
    public void createItemStock() {
        Item refItem = new StandardItem(1, "Apple", 1, "Fruits", 666);
        itemStock = new ItemStock(refItem);
    }

    @Test
    public void ItemStock_setItemToNull_shouldPass(){
//        given
        Item refItem = null;
//        when
        itemStock = new ItemStock(refItem);
//        then
        assertEquals(refItem, itemStock.getItem());
    }

    @Test
    public void ItemStock_setItemToNotNull_shouldPass(){
//        given
        Item refItem = new StandardItem(1, "Apple", 1, "Fruits", 666);
//        when
        itemStock = new ItemStock(refItem);
//        then
        assertEquals(refItem, itemStock.getItem());
    }

    @ParameterizedTest
    @MethodSource("increaseGenerator")
    public void IncreaseItemCount_testIncrease_shouldPass(int x, int expected) {
//        given
//        when
        itemStock.IncreaseItemCount(x);
        int actual = itemStock.getCount();
//        then
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("decreaseGenerator")
    public void decreaseItemCount_testDecrease_shouldPass(int x, int expected) {
//        given
//        when
        itemStock.decreaseItemCount(x);
        int actual = itemStock.getCount();
//        then
        assertEquals(expected, actual);
    }

    private static Stream<Arguments> increaseGenerator() {
        return Stream.of(
                Arguments.of(1, 1),
                Arguments.of(-1, -1),
                Arguments.of(0, 0));
    }

    private static Stream<Arguments> decreaseGenerator() {
        return Stream.of(
                Arguments.of(1, -1),
                Arguments.of(-1, 1),
                Arguments.of(0, 0));
    }
}
