package archive;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class PurchasesArchiveTest {

    private PurchasesArchive purchasesArchive;

    private static final String MOCKED_STRING_OUT = "ITEM  Item   ID 1   NAME Apple   CATEGORY Fruits   PRICE 1.0   LOYALTY POINTS 666   HAS BEEN SOLD 1 TIMES";
    private static final int ITEM_TIME_OF_SALE = 666;

    private HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive;
    private ItemPurchaseArchiveEntry mockedItemPurchaseArchiveEntry;
    private ArrayList<Order> mockedOrderArchive;

    @BeforeEach
    public void makePurchasesArchive() {
        mockedItemPurchaseArchiveEntry = Mockito.mock(ItemPurchaseArchiveEntry.class);
        itemPurchaseArchive = new HashMap<>() {{
            put(1, mockedItemPurchaseArchiveEntry);
            put(2, mockedItemPurchaseArchiveEntry);
            put(3, mockedItemPurchaseArchiveEntry);
        }};
        mockedOrderArchive = Mockito.mock(ArrayList.class);
        purchasesArchive = new PurchasesArchive(itemPurchaseArchive, mockedOrderArchive);
        when(mockedItemPurchaseArchiveEntry.toString()).thenReturn(
                MOCKED_STRING_OUT
        );
        when(mockedItemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold()).thenReturn(
                ITEM_TIME_OF_SALE
        );
    }

    @Test
    public void printItemPurchaseStatistics_correctOutput_shouldPass() {
//        given
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outContent));

        String expectedOutput = "ITEM PURCHASE STATISTICS:\r\n" +
                MOCKED_STRING_OUT + "\r\n" +
                MOCKED_STRING_OUT + "\r\n" +
                MOCKED_STRING_OUT + "\r\n";
//        when
        purchasesArchive.printItemPurchaseStatistics();
        System.setOut(originalOut);
//        then
        assertEquals(expectedOutput, outContent.toString());
    }

    @Test
    void getHowManyTimesHasBeenItemSold_findCurrentItem_shouldPass() {
//        given
        Item searchedItem = new StandardItem(1, "Apple", 1, "Fruits", 666);
//        when
        int actual = purchasesArchive.getHowManyTimesHasBeenItemSold(searchedItem);
//        then
        assertEquals(ITEM_TIME_OF_SALE, actual);
    }

    @Test
    void getHowManyTimesHasBeenItemSold_findItemThatIsNotInCollectionReturnNull_shouldPass() {
//        given
        Item searchedItem = new StandardItem(666, "Apple", 1, "Fruits", 666);
        int expected = 0;
//        when
        int actual = purchasesArchive.getHowManyTimesHasBeenItemSold(searchedItem);
//        then
        assertEquals(expected, actual);
    }

    @Test
    void putOrderToPurchasesArchive_putExistingItem_shouldPass() {
//        given
        int expectedNumberOfItemsInStock = 3;
        ArrayList<Item> items = new ArrayList<>(List.of(
                new StandardItem(1, "Apple", 1, "Fruits", 666)
        ));
        Order order = new Order(new ShoppingCart(items), null, null);
//        when
        purchasesArchive.putOrderToPurchasesArchive(order);
//        then
        Mockito.verify(mockedOrderArchive).add(order);
        Mockito.verify(mockedItemPurchaseArchiveEntry).increaseCountHowManyTimesHasBeenSold(1);
        assertEquals(expectedNumberOfItemsInStock, itemPurchaseArchive.size());
    }

    @Test
    void putOrderToPurchasesArchive_putUnknownItem_shouldPass() {
//        given
        int expectedNumberOfItemsInStock = 4;
        ArrayList<Item> items = new ArrayList<>(List.of(
                new StandardItem(666, "Watermelon", 2, "Vegetable", 69)
        ));
        Order order = new Order(new ShoppingCart(items), null, null);
//        when
        purchasesArchive.putOrderToPurchasesArchive(order);
//        then
        Mockito.verify(mockedOrderArchive).add(order);
        assertEquals(expectedNumberOfItemsInStock, itemPurchaseArchive.size());
    }
}
